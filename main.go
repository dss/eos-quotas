package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"os"
	"time"

	log "github.com/Sirupsen/logrus"
	flag "github.com/spf13/pflag"
)

// EosQuotaReport mimics the structure of the JSON output of the `eos -b --json quota ls -m` command
type EosQuotaReport struct {
	Errormsg string `json:"errormsg"`
	Quota    struct {
		Ls []struct {
			Quota               string      `json:"quota"`
			Space               string      `json:"space"`
			Statusbytes         string      `json:"statusbytes"`
			Statusfiles         string      `json:"statusfiles"`
			UID                 json.Number `json:"uid"`
			Maxbytes            uint64      `json:"maxbytes"`
			Maxfiles            uint64      `json:"maxfiles"`
			Maxlogicalbytes     uint64      `json:"maxlogicalbytes"`
			Usedbytes           uint64      `json:"usedbytes"`
			Usedfiles           uint64      `json:"usedfiles"`
			Usedlogicalbytes    uint64      `json:"usedlogicalbytes"`
			Percentageusedbytes float32     `json:"percentageusedbytes"`
			Gid                 json.Number `json:"gid"`
		} `json:"ls"`
	} `json:"quota"`
	Retc string `json:"retc"`
}

// MonitServiceMetric matches the structure described
// on the Monit docs to push service metrics
//   See: http://monitdocs.web.cern.ch/monitdocs/ingestion/service_metrics.html
type MonitServiceMetric struct {
	Producer         string   `json:"producer"`
	Type             string   `json:"type"`
	Instance         string   `json:"instance"`
	Path             string   `json:"eospath"`
	User             string   `json:"user,omitempty"`
	Group            string   `json:"group,omitempty"`
	MaxBytes         uint64   `json:"max_bytes"`
	UsedBytes        uint64   `json:"used_bytes"`
	MaxFiles         uint64   `json:"max_files"`
	UsedFiles        uint64   `json:"used_files"`
	StatusBytes      string   `json:"status_bytes"`
	StatusFiles      string   `json:"status_files"`
	PercentUsedBytes float32  `json:"percent_used_bytes"`
	IdbTags          []string `json:"idb_tags"`
	IdbFields        []string `json:"idb_fields"`
	Timestamp        int64    `json:"timestamp"`
}

// MonitBatchServiceMetrics is just a container for a list of MonitServiceMetric
type MonitBatchServiceMetrics []MonitServiceMetric

var (
	debug         bool
	test          bool
	monitEndpoint string
	timeout       int
	instance      string
)

func init() {
	flag.IntVar(&timeout, "timeout", 5, "timeout when talking to external systems")
	flag.BoolVar(&debug, "debug", false, "enable debug output")
	flag.BoolVar(&test, "test", false, "Test mode (dry-run)")
	flag.StringVar(&monitEndpoint, "endpoint", "https://monit-metrics.cern.ch/", "Endpoint to push metrics to")
	flag.StringVar(&instance, "instance", "eospps", "EOS instance name to report metrics as")

	flag.Parse()

}

func main() {
	if debug {
		log.SetLevel(log.DebugLevel)
	}

	fi, _ := os.Stdin.Stat()

	if (fi.Mode() & os.ModeCharDevice) != 0 {
		flag.Usage()
		log.Warn("This tool works better when data is piped to its STDIN...")
	}

	// Read JSON from STDIN
	raw, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		log.Error("Unable to read from STDIN:")
		log.Fatal(err)
	}

	// Parse the JSON...
	data := EosQuotaReport{}
	if err := json.Unmarshal(raw, &data); err != nil {
		log.Error("Unable to parse JSON:")
		log.Fatal(err)
	}

	batch := MonitBatchServiceMetrics{}
	ts := time.Now().UnixNano() / int64(time.Millisecond)

	// Each quota node...
	for _, node := range data.Quota.Ls {
		// Only valid ones
		if node.Statusbytes != "ignored" || node.Statusfiles != "ignored" {
			// Create Metric and prepopulate
			d := MonitServiceMetric{
				Producer:         "eos",
				Type:             "quota_usage",
				IdbTags:          []string{"instance", "eospath"},
				IdbFields:        []string{"max_bytes", "max_files", "used_bytes", "used_files", "status_bytes", "status_files", "percent_used_bytes"},
				Timestamp:        ts,
				Instance:         instance,
				Path:             node.Space,
				MaxBytes:         node.Maxlogicalbytes,
				MaxFiles:         node.Maxfiles,
				UsedBytes:        node.Usedlogicalbytes,
				UsedFiles:        node.Usedfiles,
				PercentUsedBytes: node.Percentageusedbytes,
				StatusBytes:      node.Statusbytes,
				StatusFiles:      node.Statusfiles,
			}
			if len(node.UID) > 0 {
				d.User = string(node.UID)
				d.IdbTags = append(d.IdbTags, "user")
			}
			if len(node.Gid) > 0 {
				d.Group = string(node.Gid)
				d.IdbTags = append(d.IdbTags, "group")
			}
			batch = append(batch, d)

		}
	}

	// Convert to JSON
	//jsonData, err := json.MarshalIndent(batch, "", " ")
	jsonData, err := json.Marshal(batch)

	if err != nil {
		log.Fatal(err)
	}

	if debug {
		log.Debugln(string(jsonData))
	}

	if test {
		log.Warn("Not pushing data: test-mode")
	} else {
		err = pushMetrics(jsonData)
		if err != nil {
			log.Error("Unable to push metrics:")
			log.Fatal(err)
		}
	}
}

// pushKPIs does the actual POST of the data to the monitoring infrastructure
func pushMetrics(data []byte) error {

	req, err := http.NewRequest("POST", monitEndpoint, bytes.NewBuffer(data))
	if err != nil {
		log.Error("Unable to create HTTP request: ", err)
		return err
	}
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")

	if debug {
		rd, err := httputil.DumpRequest(req, true)
		if err != nil {
			log.Error("Unable to dump HTTP Request: ", err)
		}
		log.Debug("HTTP Request:\n", string(rd))
	}

	client := &http.Client{
		Timeout: time.Duration(timeout) * time.Second,
	}
	res, err := client.Do(req)
	if err != nil {
		log.Error("Unable to POST request: ", err)
		return err
	}
	defer res.Body.Close()

	if debug {
		rd, err := httputil.DumpResponse(res, true)
		if err != nil {
			log.Error("Unable to dump HTTP Response: ", err)
		}
		log.Debug("HTTP Response:\n", string(rd))
	}

	if res.StatusCode != http.StatusOK {
		log.Error("Data not accepted by remote end: ", res.Status)
		return errors.New("Remote HTTP error code: %s" + res.Status)
	}

	return nil
}
