#!/bin/bash

INSTANCES="atlas cms lhcb public"

for k in $INSTANCES; do
  ssh root@eos${k}.cern.ch -- eos -b --json quota ls -m | ./eos-quotas
done
